+++
title = "Two different types of flow?"
date = 2020-01-28
description = "thinking out loud"
tags = ["consciousexperience", "flowstates"]
+++

[Epistemic status: first thoughts, haven't researched any of this, mainly just putting them out here to receive comments/pointers and make something out of it another day]

I believe there are two different states of mind that people often bunch together when talking about flow. I would like to tease them apart a bit as they strike me as fundamentally different in valence:

1. Dissociative states, when you’re inside the activity you're focusing on, forgetting your body and mind; and
2. Fully present moment experiences, inside your body and your mind, with your observer and actor fully active - you’re acting and experiencing your actions consciously.

I think from a “goodness” perspective, 1) often is as close to 0 as I can imagine: YOU aren't experiencing anything, YOU dissolve. 2) seems more clearly valuable for YOU, more valuable in its richness and complexity, up to the point where YOU are EVERYTHING. 1) usually allows to escape from yourself of your head. It feels good in anticipation of reducing negative experience and/or after the fact in comparison, when facing stressors. 2) allows you to experience YOUR world to its full extent.

However, 1) can lead to fully conscious in-the-moment experience - not of yourself but of whatever you’re projecting on. It just seems to me like this is not necessarily what’s happening for many people. It certainly isn’t for me, most of the time (typical mind fallacy? we’ll see).

When I get into dissociative flow states, I have two modes: autopilot or fully conscious in-the-moment experience (e.g. as the character I’m playing). But it's much harder to get into dissociative flow states with fully conscious in-the-moment experience for me. Which might point to an explanation of why it seems so hard for me to come up with random illustrations and examples, play improv or do role play. In flow, I either have autopilot or am only fully conscious and in-the-moment when I am experiencing myself, including my meta-cognition.

I am not sure about the benefits of dissociative flow - sounds kind of like addiction?