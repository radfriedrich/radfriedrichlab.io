+++
title = "Calibration app recommendation"
date = 2020-01-25
description = "friend of mine built a thing I think is useful"
tags = ["calibrationtraining", "probabilisticthinking", "bayes"]
+++

Calibration app plug: Louis Faucon recently built ["Bayes Up"](https://bayes-up.web.app/q/GD0UHIkW7TR0), which he hopes to improve further with feedback. It's based on the [Bayesian multiple choice examination idea](https://www.lesswrong.com/posts/8wzKawHmh4d3h2otw/bayesian-examination) Lê posted on LessWrong last December.