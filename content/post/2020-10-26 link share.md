+++
title = "2020-10-26 link share"
date = 2020-10-26
description = "best of my recent online reads"
tags = ["goodreads"]
+++

# Last week's top 5:

* **[Envisioning a world immune to global catastrophic biological risks](https://reflectivedisequilibrium.blogspot.com/2020/05/what-would-civilization-immune-to.html)**
> Logical extrapolation of DNA/RNA sequencing technology, physical barriers and sterilization, and robotics point the way to a world in which any new natural pandemic or use of biological weapons can be immediately contained, at increasingly affordable prices.  These measures are agnostic as to the nature of pathogens, low in dual use issues, and can be fully established in advance, and so would seem to mark an end to any risk period for global catastrophic biological risks (GCBRs).  An attainable defense-dominant 'win condition' for GCBRs means that we should think about GCBRs more in terms of a possible 'time of perils' and not an indefinite risk that sets a short life expectancy for our civilization.

* **[What do historical statistics teach us about the accidental release of pandemic bioweapons?](https://reflectivedisequilibrium.blogspot.com/2020/10/what-do-historical-statistics-teach-us.html)**
> Examining the known biological weapons programs, especially the Soviet program (by far the largest), we see that they were overwhelmingly working with diseases that were not capable of pandemic spread, with the few exceptions (particularly smallpox) subject to vaccination or having low fatality rates.  This should be expected: clearly the human population could not sustain many high fatality pandemic pathogens naturally circulating.  However, it appears that the Soviet program was engaged in active research to produce deadly pandemic pathogens, although it failed to do so with 1980s biotechnology.  If a future illegal bioweapons program were follow the Soviet example but succeed with more advanced biotechnology, historical rates of accidental release could pose a more likely threat than intentional use of the pandemic agents in warfare. 

* **[Clusters rule everything around me](https://worksinprogress.co/issue/clusters-rule-everything-around-me/)**
> As long as there are scientific and technological advancements to be made, progress on the frontier will inevitably cluster in some physical locations. Until we humans are plugging our brains directly into computers, we remain social animals who need physical interaction to best stimulate, develop, and actively work out new ideas. The highest digital worlds we can create will rely on the careful curation of our physical one. Pretending otherwise is to delay the difficult policy work that needs to be done to start maximizing our tech clusters instead of actively undermining them as we’ve been doing for decades. 

* **[Are the Neurotypicals Okay?](https://applieddivinitystudies.com/optional-fashion/)**
> The mistake where you’re miserable, in a job you hate, wearing clothes that make you uncomfortable, scrunching your feet into small shoes, stuck in a distracting environment, and yet somehow, manage to convince yourself that everything is fine. That’s neurotypical behavior, and maybe it’s an optimal contextualization of sensory information for some purposes, but it also just sucks. It’s a failure of self-awareness and self-efficacy. In this model, neurotypicality is better understood as a generalized inability to understand how your environment is impacting you, and then take actions to improve it.

* **[Securing posterity](https://worksinprogress.co/issue/securing-posterity/)**
> To the extent that we are at a crossroads between continued growth with—potentially risky—innovation on the one hand, and stagnant decadence in the name of comfort and safety on the other hand, those concerned for posterity should respond with a unified voice: we choose growth.

# and boni:

* **[Professional Development in Operations](https://docs.google.com/document/d/1FpU9bscJrZQZioDQKZ8tFcTbR_BG7K82Z1FBd5dfvSg/)** by Daniel Kestenholz:
> This is a roadmap on how to skill-up in an operations role. See this article for an introduction to the field. Get in touch if you’re looking to transition into such a role or are seeking mentoring.

* **[Personal Productivity](https://docs.google.com/document/d/1VJ60DgUCTuS9kgB2Ylu_JY9sMnOxkK0f6p4OiSSYqVQ/)** by Daniel Kestenholz:
> This is a collection of methods and tools to experiment with and reading material to study in the context of personal productivity. This is a collaborative document, so please add anything that you’ve found helpful. 

* **[https://twitter.com/davidklaing/status/1316215365995687936](https://twitter.com/davidklaing/status/1316215365995687936)**
> When writing, I'm usually tempted to work on one sentence at a time, only moving on when the previous sentence feels perfect. But I find I work better when I take a blurry-to-sharp approach: outline, then sub-outlines, then rough prose, then polished prose.

* **[The relationship between intelligence and creativity: New support for the threshold hypothesis by means of empirical breakpoint detection](https://www.sciencedirect.com/science/article/pii/S016028961300024X)**
> When investigating a liberal criterion of ideational originality (i.e., two original ideas), a threshold was detected at around 100 IQ points. In contrast, a threshold of 120 IQ points emerged when the criterion was more demanding (i.e., many original ideas). Moreover, an IQ of around 85 IQ points was found to form the threshold for a purely quantitative measure of creative potential (i.e., ideational fluency). […] no threshold was found for creative achievement, i.e. creative achievement benefits from higher intelligence even at fairly high levels of intellectual ability.

* **[Poverty, Depression, and Anxiety: Causal Evidence and Mechanisms](https://gautam-rao.com/pdf/Ridley%20et%20al%20-%202020%20-%20Poverty%20Depression%20Anxiety.pdf)**
> Why are people living in poverty disproportionately affected by mental illness? We review the interdisciplinary evidence of the bi-directional causal relationship between poverty and common mental illnesses – depression and anxiety – and the underlying mechanisms. Research shows that mental illness reduces employment and therefore income and that psychological interventions generate economic gains. Similarly, negative economic shocks cause mental illness, and anti-poverty programs such as cash transfers improve mental health.

* **[Embracing asynchronous communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/#async-30-at-gitlab)** (+ [discussion on HN](https://news.ycombinator.com/item?id=24800006))
> In a world dictated by calendars and schedules, people are conditioned to operate in synchronicity — a manner in which two or more parties exert effort to be in the same place (either physically or virtually) at the same time. Asynchronous communication is the art of communicating and moving projects forward without the need for additional stakeholders to be available at the same time your communique is sent.

* **[HN Comment Thread: Firms That Imploded Have Something in Common: Ernst and Young Audited Them](https://news.ycombinator.com/item?id=24802741)**
> These firms effectively operate as a collective for the benefit of the partner class. New partners must invest capital, ongoing partners get returns, and older partners retire with a terminal payout. Some partners may take on expanded leadership roles and get higher annual payouts.

* **[500 Million, But Not a Single One More](https://blog.jaibot.com/500-million-but-not-a-single-one-more/)**
> This one evil, the horror from beyond memory, the monster that took 500 million people from this world – was destroyed. You are a member of the species that did that. Never forget what we are capable of, when we band together and declare battle on what is broken in the world.

* **[Knowledge Bootstrapping Steps v0.1](https://acesounderglass.com/2020/07/06/knowledge-bootstrapping-steps-v0-1/)**
> The goal of this system is to turn questions into answers. It does this by collecting questions and assembling data that relate to them until you can come to a confident answer, or at least an answer with a confidence interval.  This system has worked amazingly well for me, but I am one person. I am very sure that 

* **[Fat-Tailed Uncertainty in the Economics of Catastrophic Climate Change](https://scholar.harvard.edu/files/weitzman/files/fattaileduncertaintyeconomics.pdf)**
> Taking fat tails into account has implications for climate change research and policy. For example, perhaps more emphasis should be placed on research about the extreme tails of relevant [probability density functions] rather than on research about central tendencies. As another example, the fatness of the bad fat tail of proposed solutions (such as, perhaps, the possibility that buried CO2 might escape) needs to be weighed against the fatness of the tail of the climate change problem itself. With fat tails generally, we might want more explicit contingency planning for bad outcomes, including, perhaps, a niche role for last-resort portfolio options like geoengineering.

* **[Innovation is not linear](https://worksinprogress.co/issue/innovation-is-not-linear/)**
> As we think about new models of funding and organization to address stagnation and revitalize progress, we should recognize that science and invention are tightly intertwined and mutually reinforcing, and seek models that integrate them more closely. Projects, programs, and institutions—and the funding that backs them—should not be siloed into one category or the other. And there should be a career path for researchers who want their work to encompass both discovery and practical applications, and who are ready, willing and able to nimbly hop from one to the other while following the scent of a breakthrough. Historically, at least, this appears to be the way to get more plastics, vaccines, and transistors.

* **[Is climate change an “existential threat” — or just a catastrophic one?](https://www.vox.com/future-perfect/2019/6/13/18660548/climate-change-human-civilization-existential-risk)**
> Climate change won’t kill us all. That matters. Yet it’s one of the biggest challenges ahead of us, and the results of our failure to act will be devastating. That message — the most accurate message we’ve got — will have to stand on its own.

* **[Escaping science’s paradox](https://worksinprogress.co/issue/escaping-sciences-paradox/)**
> Improving reproducibility and innovation isn’t easy, to be sure. But science policy and science funders could do both at once by demanding more null results, and by substantially funding efforts to contradict groupthink and confirmation bias. And this would help all of society get more value out of the many billions of dollars that we collectively spend on science every year.

* **[Estimating the Philanthropic Discount Rate](https://forum.effectivealtruism.org/posts/3QhcSxHTz2F7xxXdY/estimating-the-philanthropic-discount-rate)**
> In this essay, I have reviewed a number of philanthropic opportunities that, according to the simplistic Ramsey model, could substantially improve the world. Some of these are already widely discussed in the EA community, others receive a little attention, and some are barely known at all. These opportunities include: Reducing existential risk. Reducing individual value drift. Improving the ability of individuals to delegate their income to value-stable institutions. Making expropriation and value drift less threatening by spreading altruistic funds more evenly across actors and countries. Reducing the institutional value drift/expropriation rate. More accurately estimating the discount rate in order to know how best to use resources over time.

