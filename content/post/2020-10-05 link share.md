+++
title = "2020-10-05 link share"
date = 2020-10-05
description = "best of my recent online reads"
tags = ["goodreads"]
+++

# This week's top 5:

* **["What's a rule that was implemented somewhere that massively backfired?"](https://twitter.com/TrungTPhan/status/1311300651645767682)**
> Here are the best ones: 8/ In Athens (late 80s), the government tried to limit pollution by having odd-numbered and even-numbered license plates drive on alternating days. Result: everyone bought a second (shittier and with worse emission) car as their backup. Streets stayed clogged, pollution worse.

* **[Think of half-arsing as technical debt](https://news.ycombinator.com/item?id=24563822)**
> when you buy a house, you don't use cash. You borrow. Debt is good as long as you pay it off, and don't get smothered in accumulated interest. Similarly, when you're starting to prototype a complex system with limited time and people resources, it's better to put in working kludges than to attempt a "really good job" on every single component. It's like "perfect is the enemy of good", but crappier. More like "good is the enemy of meh-it'll-do".

* **[Information about action outcomes differentially affects learning from self-determined versus imposed choices](https://www.nature.com/articles/s41562-020-0919-5.epdf?sharing_token=p2aWVBxe7fPnQRbKKvh-v9RgN0jAjWel9jnR3ZoTv0Mie4auGBz6CxlUUnwPba2IlVzLgiczdPXDhk6U_dW7wtx7WbdygpHyuNjA0bEuD5HshB0jpIzobUKQoyz8fDTMMNpp54N-2e6exaASrCS_Rf139kMxaVCmL-iYI5FO7Lr_IeKP7V46Q5fFBxXQFxsCTaPejTAGT6-GQNsnpLtaxdwUDt7GZVzHIHwZxIXYYAheAStmcyeRcQSOm4JmthkK9UceCtfyjkMe6pw1AN0RKr9WNv6UeAtLpnFd45zYqi-6ZG1-8CuQ_yB-pTZvt9WL)**
> The valence of new information influences learning rates in humans: good news tends to receive more weight than bad news. We investigated this learning bias in four experiments, by systematically manipulating the source of required action (free versus forced choices), outcome contingencies (low versus high reward) and motor requirements (go versus no-go choices). Analysis of model-estimated learning rates showed that the confirmation bias in learning rates was specific to free choices, but was independent of outcome contingencies. The bias was also unaffected by the motor requirements, thus suggesting that it operates in the representational space of decisions, rather than motoric actions. Finally, model simulations revealed that learning rates estimated from the choice-confirmation model had the effect of maximizing performance across low- and high-reward environments. We therefore suggest that choice-confirmation bias may be adaptive for efficient learning of action–outcome contingencies, above and beyond fostering person-level dispositions such as self-esteem.

* **[Alexey Guzey: Why I didn't do more - an email I sent today](https://mobile.twitter.com/alexeyguzey/status/1312063416807419906)**
> Correct me if I'm wrong, but I believe you come from a very good and supportive family, you go to a good high school, and what I would've liked to see is output significantly above what is expected given those inputs. [...] Specifically, the thing that I'm usually looking for is some sort of an independent project that demonstrates that its creator is exceptional and that could not have been done as part of the "standard curriculum" and where it would be clear that it's the author's ideas and vision  (and not teachers', parents' or mentors') that I'm looking at. In your case, the bar that the standard curriculu creates is rather high.

* **[The hidden links between mental disorders](https://www.nature.com/articles/d41586-020-00922-8)**
> Every single mental disorder predisposed the patient to every other mental disorder — no matter how distinct the symptoms1. “We knew that comorbidity was important, but we didn’t expect to find associations for all pairs,” says Plana-Ripoll, who is based at Aarhus University in Denmark. The study tackles a fundamental question that has bothered researchers for more than a century. What are the roots of mental illness?

# Boni:

* **[This Overlooked Variable Is the Key to the Pandemic](https://www.theatlantic.com/health/archive/2020/09/k-overlooked-variable-driving-pandemic/616548/)** - neat illustration of complex systems and how many people still think about them in ways that aren't very productive:
> By now many people have heard about R0—the basic reproductive number of a pathogen, a measure of its contagiousness on average. But unless you’ve been reading scientific journals, you’re less likely to have encountered k, the measure of its dispersion. The definition of k is a mouthful, but it’s simply a way of asking whether a virus spreads in a steady manner or in big bursts, whereby one person infects many, all at once. After nine months of collecting epidemiological data, we know that this is an overdispersed pathogen, meaning that it tends to spread in clusters, but this knowledge has not yet fully entered our way of thinking about the pandemic—or our preventive practices.

* **[Ben Todd's answer to "The ITN (Impact, Tractability, Neglectedness) framework is widely used in EA to do cause prioritisation . Which could be other metrics that we might be missing out on? Is the ITN framework exhaustive?"](https://forum.effectivealtruism.org/posts/YxgDypoHSPYhhfcMx/factors-other-than-itn)**
> INT = good per dollar by definition (when used in the quantitative rather than heuristic way), so in that sense it's exhaustive, though in practice, people often miss some factors that are not as naturally captured by the framework:

* **[On Being Okay with the Truth](https://www.lesswrong.com/posts/3v24wGePcdSGB3i8a/on-being-okay-with-the-truth)**
> We humans have a tendency to 'freak out' when our model of the world changes drastically. But we get over it. The love a mother has for her child does not disappear when we explain the brain processes that instantiate that love. Explaining something is not explaining it away. Showing that love and happiness and moral properties are made of atoms does not mean they are just atoms. They are also love and happiness and moral properties. Water was still water after we discovered which particular atoms it was made of. When you understand this, you need not feel the threat of nihilism as science marches on. Instead, you can jump with excitement as science locates everything we care about in the natural world and tells us how it works. Along the way, you can take joy in the merely real. Whenever you 'lose' something as a result of getting closer to the truth, you've only lost a lie. You can face reality, even the truth about morality.

* **[Becoming Eden - Start Here](http://becomingeden.com/start-here/)** (a great blog on parenting, productivity and [preventing peanut allergies](http://becomingeden.com/preventing-peanut-allergies/))
> In addition to being a(n admittedly occasional) blog, Becoming Eden is also a repository of a number of summaries, essays, and other longer pieces we like to reference periodically. Here is an up-to-date index of our major posts:

* **[In Defence of Half-Arsing](https://drmaciver.substack.com/p/in-defence-of-half-arsing)**
> We spend too much of our formative years in an environment where there is an external arbiter who will punish us for not doing the thing “properly”, and does not allow us to use our judgement as to what counts as good enough. Too often this causes us to acquire an inappropriate degree of perfectionism, where our only options for half-arsery are to either convince ourselves that the thing is unimportant, or we procrastinate until we have no choice but to half-arse in order to get things done in time.

* **[HN comment thread illustrating the complexity of justice systems (and the difficulties of making it accessible)](https://news.ycombinator.com/item?id=24562115)**
> Short of a periodic collapse and simplification of case law such that the entire field can become more accessible so something less than a Guild/Cartel of highly exclusive barristers, I'm not seeing a fundamental opportunity to change things for the better.

* **[A Modest Proposal - a tongue-in-cheek suggestion for a new unit of currency](https://www.gwern.net/docs/philo/2011-yvain-deadchild.html)**
> The past two years, I've spent about two dead puppies on books from Amazon.com alone. I am probably going to spend very close to a whole dead child to fly home for my two week winter break, and I spent ten dead children on my trip around the world this summer. I spent four infected wounds on fantasy map-making software. But at least in the back of my mind I realize I'm doing it. Can the people who spend a dead kid plus a dead puppy on the world's most expensive sundae say the same? What about the Japanese guy spending 1050 dead kids on a mobile phone strap?

* **[Positive vs. Optimal](https://web.archive.org/web/20101015213928/http://www.overcomingbias.com/2008/11/positive-vs-opt.html)**
> Our intuition is a terrible task prioritizer. And much of the erroneous analysis about the benefits of regulation has to do with ignoring the invisible (the best alternative use of the resources), as Henry Hazlitt so eloquently writes. Our intuition seizes on the visible consequences, and has trouble seeing the subtle, distributed, unrealized, un-proposed alternatives. Which suggests a technique for overcoming this, at both the personal and professional levels. Try to always present alternatives. Reify the other options – or your mind will focus on whether your proposal does net good, rather than the most good with its limited resources.

* **[Most favorited Hacker News posts of all time](https://observablehq.com/@tomlarkworthy/hacker-favourites-analysis)** (and [a slightly different version of this list](https://news.ycombinator.com/item?id=24356341))
> The most favorited articles by the top 10k most active Hacker News members. The list skews toward innovative learning resources and tech career tips, but there is a little of everything.

* **[Being A Noob](http://paulgraham.com/noob.html)**
> It made sense for humans to dislike the feeling of being a noob, just as, in a world where food was scarce, it made sense for them to dislike the feeling of being hungry. Now that too much food is more of a problem than too little, our dislike of feeling hungry leads us astray. And I think our dislike of feeling like a noob does too.

* **[What Doesn't Seem Like Work?](http://paulgraham.com/work.html)**
> The stranger your tastes seem to other people, the stronger evidence they probably are of what you should do. [...] So I bet it would help a lot of people to ask themselves about this explicitly. What seems like work to other people that doesn't seem like work to you?