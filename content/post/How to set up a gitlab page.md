+++
title = "How to set up a gitlab page"
date = 2020-10-05
description = "the steps I went through to start this thing, including some of the issues I encountered"
tags = ["gitlab", "hugo", "blog"]
+++

I spent two Sunday afternoons figuring out how to get a blog started. You can do it in less than an hour. Here's the workflow that did the job in the end.

Epistemic status: No single guide covered everything I needed to know. So here's my likely-incomplete guide just in case it might help somebody else (e.g. future me).

<!--more-->

1. Follow [Hugo's hosting on gitlab guide](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/)
	- If you're a newb like me and use Windows, [here's how to create the .gitlab-ci.yml file within the Command Prompt](https://www.wikihow.com/Create-and-Delete-Files-and-Directories-from-Windows-Command-Prompt)
	- Here's [an ok (too hype-y) guide I found on how to get started with Git](https://towardsdatascience.com/getting-started-with-git-and-github-6fcd0f2d4ac6)
	- GitLab's "Pages" seems less complicated than GitHubs, so maybe ignore GitHub?
2. Move the folder of the theme into the 'themes' folder (rename the folder of the theme to just the name of the theme) and then reference it correctly within the '.gitmodules' file ('path = themes/themename')
3. There are likely to be errors - they probably have to do with the template, not with your work so far. I tried three themes - 'fuji' worked ('pure' and 'stack' did not). I recommend simply using a different template (you can simply use 'git submodule add <link to your template>" again and delete the old template.
4. Make sure your gitlab project's path is your URL ('radfriedrich.gitlab.io') if you want it there (otherwise it's at 'radfriedrich.gitlab.io/projectname')
5. Move the 'config.toml' from your theme's 'exampleSite' folder to your root folder and set up your page (e.g. set 'baseURL' to the URL of your site; set the theme; write a description and other bits of key info)
6. Create your first post in a *.md file in the 'content' folder and you're ready to go.