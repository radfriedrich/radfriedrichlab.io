
+++
title = "2020-10-12 link share"
date = 2020-10-12
description = "best of my recent online reads"
tags = ["goodreads"]
+++

# This week's top 5

* **[When Evolution Is Infectious](http://nautil.us/issue/90/something-green/when-evolution-is-infectious-rp)**
> new technologies have allowed the study of microbial communities to flourish, and made it abundantly clear that macrobes are not, and never have been, alone. In this context, the hologenome idea, and the occurrence of probiotic epidemics, represent an attempt to explicitly include microbes, the dominant life form on the planet, in evolutionary theory. Understanding how microbes aid survival could have far-reaching applications, ranging from new angles on wildlife conservation to improvement of human health.

* **[Inside the House Report on Big Tech and Competition: Part II](https://diff.substack.com/p/inside-the-house-report-on-big-tech-6a6)**
> The economics of software reward scale, but generally because software products are better at scale. Google resolves long-tail queries well; two half-Googles would each be less than half as good at this. Facebook connects people, and two half-Facebooks would have fewer than half the connections (and would be able to support far smaller moderation staffs, and might feel more pressure to break end-to-end encryption, allow more dubious advertisers, etc.). Those wonderful scale-and-network-effect economics depend on fragmentation one level up and down the supply chain, so they’re always a tempting target for competitors, and the profitability of a mature tech company creates a public-market comp for funding incumbents.

* **[The deep Anthropocene](https://aeon.co/essays/revolutionary-archaeology-reveals-the-deepest-possible-anthropocene)**
> Humans have continually altered biodiversity on many scales. We have changed the local mix of species, their ranges, habitats and niches for thousands of years. Long before agriculture, selective human predation of many non-domesticated species shaped their evolutionary course. Even the relatively small hunter-gatherer populations of the late Pleistocene were capable of negatively affecting animal populations – driving many megafauna and island species extinct or to the point of extinction. But there have also been widespread social and ecological adaptations to these changes: human management can even increase biodiversity of landscapes and can sustain these increases for thousands of years.

* **[Notes about “Data Science and the Decline of Liberal Law and Ethics”](https://digifesto.com/2020/07/06/notes-about-data-science-and-the-decline-of-liberal-law-and-ethics/)**
> or (c) it is so hard for any individual to conceive of corporate cognition because of how it exceeds the capacity of human understanding that speaking in this way sounds utterly speculative to a lot of fo people. The problem is that it requires attributing cognitive and adaptive powers to social forms, and a successful science of social forms is, at best, in the somewhat gnostic domain of complex systems research.

* **[The real lessons from Sweden’s approach to covid-19](https://www.economist.com/leaders/2020/10/10/the-real-lessons-from-swedens-approach-to-covid-19)**
> The lesson from the new Swedish policy is not that it is libertarian, but that the government weighs up the trade-offs of each restriction. For instance, when someone tests positive, their entire household must go into quarantine, but schoolchildren are exempt—because, the government reckons, the gains from shutting them away are overwhelmed by the lasting harm to their education. Likewise, the quarantine lasts five to seven days, compared with two weeks elsewhere. The risk of spreading covid-19 in that second week is small and shrinking, but the harm to mental health of extended isolation is growing.

## And boni

* **[Why Life Can’t Be Simpler](https://fs.blog/2020/10/why-life-cant-be-simpler/)**
> When we wish for things to be simpler, we usually mean we want products and services to have fewer steps, fewer controls, fewer options, less to learn. But at the same time, we still want all of the same features and capabilities. These two categories of desires are often at odds with each other and distort how we understand the complex.

* **[Humans Are All More Closely Related Than We Commonly Think](https://www.scientificamerican.com/article/humans-are-all-more-closely-related-than-we-commonly-think/)**
> The consequence of humanity being “incredibly inbred” is that we are all related much more closely than our intuition suggests, Rutherford says. Take, for instance, the last person from whom everyone on the planet today is descended. In 2004 mathematical modeling and computer simulations by a group of statisticians led by Douglas Rohde, then at the Massachusetts Institute of Technology, indicated that our most recent common ancestor probably lived no earlier than 1400 B.C. and possibly as recently as A.D. 55. In the time of Egypt’s Queen Nefertiti, someone from whom we are all descended was likely alive somewhere in the world.

* **[Predictive Coding Approximates Backprop along Arbitrary Computation Graphs](https://openreview.net/forum?id=PdauS7wZBfC)**
> The backpropagation of error (backprop) is a powerful algorithm for training machine learning architectures through end-to-end differentiation. Recently it has been shown that backprop in multilayer-perceptrons (MLPs) can be approximated using predictive coding, a biologically-plausible process theory of cortical computation which relies solely on local and Hebbian updates. [...] Our method raises the potential that standard machine learning algorithms could in principle be directly implemented in neural circuitry, and may also contribute to the development of completely distributed neuromorphic architectures.

* **[Do algorithms reveal sexual orientation or just expose our stereotypes?](https://medium.com/@blaisea/do-algorithms-reveal-sexual-orientation-or-just-expose-our-stereotypes-d998fafdf477)**
> We are hopeful about the confluence of new, powerful AI technologies with social science, but not because we believe in reviving the 19th century research program of inferring people’s inner character from their outer appearance. Rather, we believe AI is an essential tool for understanding patterns in human culture and behavior.

* **[After 40 years, researchers finally see Earth’s climate destiny more clearly](https://www.sciencemag.org/news/2020/07/after-40-years-researchers-finally-see-earths-climate-destiny-more-clearly)**
> “The real advantage” of Bayesian statistics, Tierney says, is how it allows uncertainties at each stage to feed into a final result. Co-authors often butted heads, Marvel says. “It was such a long and painful process.” The final range represents a 66% confidence interval, matching IPCC’s traditional “likely” range. The WCRP team also calculated a 90% confidence interval, which ranges from 2.3°C to 4.7°C, leaving a slight chance of a warming above 5°C.

* **[3 suggestions about jargon in EA](https://forum.effectivealtruism.org/posts/uGt5HfRTYi9xwF6i8/3-suggestions-about-jargon-in-ea)**
> I suggest that effective altruists should: (a) Be careful to avoid using jargon to convey something that isn’t what the jargon is actually meant to convey, and that could be conveyed well without any jargon. As examples, I’ll discuss misuses I’ve seen of the terms existential risk and the unilateralist’s curse, and the jargon-free statements that could’ve been used instead. (2) Provide explanations and/or hyperlinks to explanations the first time they use jargon. (3) Be careful to avoid implying jargon or concepts originated in EA when they did not.

