+++
title = "2020-10-19 link share"
date = 2020-10-19
description = "best of my recent online reads"
tags = ["goodreads"]
+++

# Last week's top 5:

* **[Why might the future be good?](https://rationalaltruist.com/2013/02/27/why-will-they-be-happy/)**
> I often hear people talking about the future, and the present for that matter, as if we are falling towards a Darwinian attractor of cutthroat competition and vanishing empathy (at least as a default presumption, which might be averted by an extraordinary effort). I think this picture is essentially mistaken, and my median expectation is that the future is much more altruistic than the present.

* **[Smaller Crowds Outperform Larger Crowds and Individuals in Realistic Task Conditions](https://drive.google.com/file/d/0B4Hb4fyv4iVJbm1oTjk0ZEplaG8/view)**
> Decisions about political, economic, legal, and health issues are often made by simple majority voting in groups that rarely exceed 30–40 members and are typically much smaller. Given that wisdom is usually attributed to large crowds, shouldn’t committees be larger? In many real-life situations, expert groups encounter a number of different tasks. Most are easy, with average individual accuracy being above chance, but some are surprisingly difficult, with most group members being wrong. Examples are elections with surprising outcomes, sudden turns in financial trends, or tricky knowl- edge questions. Most of the time, groups cannot predict in advance whether the next task will be easy or difficult. We show that under these circumstances moderately sized groups, whose members are selected randomly from a larger crowd, can achieve higher average accuracy across all tasks than either larger groups or individuals. This happens because an increase in group size can lead to a decrease in group accuracy for difficult tasks that is larger than the corresponding increase in accuracy for easy tasks. We derive this nonmonotonic relationship between group size and accuracy from the Condorcet jury theorem and use simulations and further analyses to show that it holds under a variety of assumptions. We further show that situations favoring moderately sized groups occur in a variety of real-life situations including political, medical, and financial decisions and general knowledge tests. These results have implications for the design of decision-making bodies at all levels of policy.

* **[What Helped the Voiceless? Historical Case Studies](https://docs.google.com/document/d/1otYfuyIqBlOpTgUBvQa-nowYUh1TxEx5-xJpCbY8Fto/)**
> Future generations, non-human animals, and other voiceless groups are harmfully neglected in today’s policy making. What strategies for changing this can advocates of neglected groups learn from times when excluded groups gained political protections?

* **[The Bus Story](https://www.facebook.com/notes/duncan-sabien/the-bus-story/3782021111832578/)**
> And interestingly, the bully zone just sort of...receded.  Slowly.  Without anybody really doing anything on purpose.  On most of the other buses (I rode them from time to time, when our bus broke down or when I was going to or from a friend’s house or whatever), the safe zone was just the front four rows of seats.

* **[Polyamory: the pot of gold](https://standrewsradio.com/polyamory-the-pot-of-gold/)**
> To be held by someone you love while holding the hands of another; to watch someone you love experience joy even when you’re not around to give it; to talk with shining eyes about your new love with an old one, and watch their eyes shine back at you… these are the things that make that journey through jealousy worth it. A transition that requires patience, introspection, self-reflection, and a willingness to acknowledge things about yourself that you might find a little painful, but that brings with it a whole philosophy of life and freedom.

# and boni:

* **[Experience curves, large populations, and the long run](https://reflectivedisequilibrium.blogspot.com/2020/05/experience-curves-large-populations-and.html)**
> If our civilization avoids catastrophe, will we generally be able to advance technology close to physical limits, match or exceed observed biological abilities, and colonize the universe? Or will we be stuck in a permanent technological plateau before that, reaching a state where resources are insufficient to make the breakthroughs to acquire resources and continue progress? 

* **[Twitter-thread:](https://mobile.twitter.com/AmandaAskell/status/1315764087192653824)**
> What are the biggest differences you've noticed between your inner life and the inner life of others? E.g. having/not having: mental images, an inner monologue, a felt sense of gender, control over feelings, an ability to hear imagined sounds/music, a need for solitude.

* **[DMT alters cortical travelling waves](https://elifesciences.org/articles/59784)**
> These results support a recent model proposing that psychedelics reduce the 'precision-weighting of priors', thus altering the balance of top-down versus bottom-up information passing. The robust hypothesis-confirming nature of these findings imply the discovery of an important mechanistic principle underpinning psychedelic-induced altered states.

* **[Politically Incorrect Paper of the Day: The Persistence of Pay Inequality](https://marginalrevolution.com/marginalrevolution/2020/10/politically-incorrect-paper-of-the-day-the-persistence-of-pay-inequality.html)**
>> In this study we examined the gender pay gap on an anonymous online platform across an 18-month period, during which close to five million tasks were completed by over 20,000 unique workers. Due to factors that are unique to the Mechanical Turk online marketplace–such as anonymity, self-selection into tasks, relative homogeneity of the tasks performed, and flexible work scheduling–we did not expect earnings to differ by gender on this platform. However, contrary to our expectations, a robust and persistent gender pay gap was observed. 

> In this case, however, neither experience nor task choice nor demographics appears to explain the difference. One interesting finding is that women are more likely to choose tasks with a lower advertised pay–perhaps men are just a bit lazier. Who knows? People are different.

* **[Japan to release Fukushima's contaminated water into sea: reports]()** (note: this basically is good news)
> In April, a team sent by the International Atomic Energy Agency to review contaminated water issues at the Fukushima site said the options for water disposal outlined by an advisory committee in Japan - vapour release and discharges to the sea – were both technically feasible. The IAEA said both options were used by operating nuclear plants.

* **[Avoiding Munich's Mistakes: Advice for CEA and Local Groups](https://forum.effectivealtruism.org/posts/zYNDJxDm4tWgw8AWn/avoiding-munich-s-mistakes-advice-for-cea-and-local-groups)**
> For the same reasons I think it is bad to deplatform a speaker as the result of a vicious cancel culture attack, I think it would be bad to not invite them in the first place for these reasons. There are many acceptable reasons not to invite someone - like timing, or relevance, or having a full schedule, or simple being unaware of their existence. But appeasing cancel culture is not one of them.

* **[Hacker News discussion](https://news.ycombinator.com/item?id=24800403)** of [Babies' random choices become their preferences](https://hub.jhu.edu/2020/10/02/babies-prefer-what-they-choose-even-when-random/) ([og paper here](https://journals.sagepub.com/doi/abs/10.1177/0956797620954491?journalCode=pssa))
> You absolutely can and should shape your identity around what type of person you want to become in the future. You can objectively look at your likes and behaviors and decide to replace them. If you're on the spectrum, like I suspect a lot of HN readers are, in some ways it's even easier to take this kind of detached view of yourself. Personally, I used to really like X, it was a core part of my identity. One day I simply decided to not do X anymore, I didn't like the people doing X and I didn't see any successful people in my orbit, and I replaced it with Y & Z. It changed the type of people I was around, it changed my career path, and my core personality. Just like you can choose to work on your computer, house, you car, etc. you can choose to work on your identity, personality and likes.