+++
title = "Notes on IQ and Creativity"
date = 2020-10-25
description = "mostly a collection of evidence"
tags = ["intelligence", "creativity"]
+++

Last updated: 2020-10-26

[Epistemic status: a collection of studies and comments pointing into a pretty clear direction. Some meta-analyses, some not. Haven't found counter evidence but field generally doesn't seem to be of best research quality. I suppose it's hard to get funding/a good career for potenitally extremely controversial research.]

# [Hacker News](https://news.ycombinator.com/item?id=18755193&ref=hvper.com) re [Taleb on IQ](https://medium.com/incerto/iq-is-largely-a-pseudoscientific-swindle-f131c101ba39), comments giving a good sense for what Taleb misses:
- IQ is a statistical parameter (offering no information whatsoever about individuals) but this smart guy quotes local anecdotes and moreover from the vantage point of an environment populated by outliers.
- The point was that __Taleb__ argues it has no value (above ~85), and then says it’s a good predictor of one’s value as a “slave” (very high IQ makes a good slave). It’s all filtered through the idea that success means being a multimillionaire, and everyone else is a failure. His perspective seems limited to finance and academics in that sphere.
- __If many millionaires have IQs around 100, & 58 y.o. back office clercs at Goldman Sachs or elsewhere an IQ of 155 (true example), clearly the measurement is less informative than claimed.__
    - it is important to distinguish the macro from the micro point of view.
    - From the micro point of view, a talented individual has a greater a priori probability to reach a high level of success than a moderately gifted one.
    - On the other hand, from the macro point of view of the entire society, the probability to find moderately gifted individuals at the top levels of success is greater than that of finding there very talented ones, because moderately gifted people are much more numerous and, with the help of luck, have - globally - a statistical advantage to reach a great success, in spite of their lower individual a priori probability.

# A friend of mine read through Taleb's post, trying to quickly isolate his key points (second-level bullets are my replies)
- IQ only correlates with success in the same things that IQ tests for (what he calls IYI, things like quants, normal engineers etc, book smarts basically)=> circularity
    - That just seems to be straight-up bullshit. It sounds to me like he doesn't even know what IQ tests are and how they work (Raven vs WAIS, crsytallized g vs fluid g etc.). Arguing against this point is not worth it here because it's so obvious from everything below, so bear with me.
- IQ only has predictive power for mental handicaps and close to average, once you are mentally fit it doesn't work well to capture any interesting information except "not below baseline"
    - In his own article, he shows a graph (of which I cannot retrieve the source) of correlation of SAT and IQ scores that clearly demonstrate correlation up to at least IQ 130 (~2SD, depending on exact test)
    - For many things, correlation only holds until ~115 but pretty clearly not for income. Here we see a linear relationship up until at least 130. https://sci-hub.se/https://doi.org/10.1016/j.intell.2018.10.002
    - IQ even positively correlates with *not* getting into car accidents, which seems pretty solidly useful for Taleb's much loved survival https://sci-hub.se/10.1126/science.299.5604.192
- IQ tests test for some specific tasks wich a) are trainable and b) have limited significance to real life tasks
    - They are only trainable to some extent - your cognitive functioning is still going to limit your performance. That's why full WAIS tests take ~4-8 hours with a psychometrician and are mostly verbal - you will have to get used to the exercises etc and the speed at which you do is relevant. That's also why it makes sense to take multiple tests if you want to obsess over exact numbers (which seems unnecessary, other than for statistical robustness imo).
    - I'm not sure how it matters whether the tests are not relevant to real life tasks if what they measure underlies general performance across almost everything we care about?
- performance is heavy tailed while IQ tests are gaussian -- something about tranlsating between them is weird
    - Some things are normally distributed. It seems pretty well established that a lot of physiological human features are - and IQ seems to mirror things like neural efficiency (https://www.sciencedirect.com/science/article/abs/pii/S092664100500162X)and grey matter volume and connectivity (https://sci-hub.se/10.1371/journal.pone.0112691) rather well.
- the variance bewteen test and retest is high, which lowers the predictive power of IQ tests between subjects
    - This is at the individual level, yes, but from the macro perspective at the population-level it works out. Meaning that yes, for you personally your final IQ number isn't relevant (but the proper WAIS test still likely to be highly insightful) BUT if you have to choose between a more or less intelligent population, you want the more intelligent one (cf HN comment above).
- the National IQ is a Fraud thing about neglecting variance between popluations (though that's less a critique of IQ tests per se but of some people not undestanding statistics)
    - I'm not sure what this means

# Basics re IQ
### What is IQ & G-factor
- G is essentially the ability to deal with complex information processing. [...] These and other data are summarized to illustrate how the advantages of higher g, even when they are small, cumulate to affect the overall life chances of individuals at different ranges of the IQ bell curve. https://www1.udel.edu/educ/gottfredson/reprints/1997whygmatters.pdf
- meta-analyses of hundreds of studies have demonstrated that IQ is predictive of life success across many domains. The use of IQ tests can help us predict things we want to predict and to explain things we want to explain. https://www.gwern.net/docs/iq/2015-strenze.pdf
- Understanding the Nature of the General Factor of Intelligence: The Role of Individual Differences in Neural Plasticity as an Explanatory Mechanism https://sci-hub.se/https://psycnet.apa.org/doiLanding?doi=10.1037/0033-295X.109.1.116
    - A measure of cognitive ability, known as g, has been shown highly heritable across many studies. We argue that these genetic links are partly mediated by brain structure that is likewise under strong genetic control. Other factors, such as the environment, obviously play a role, but the predominant determinant appears to genetic. https://www.annualreviews.org/doi/abs/10.1146/annurev.neuro.28.061604.135655
### Physical properties of the brain and IQ
- The association between brain volume and intelligence (‘g’) was r = 0.276. https://www.sciencedirect.com/science/article/pii/S0160289619300789
- grey matter volume and connectivity (https://sci-hub.se/10.1371/journal.pone.0112691)
- grey matter volume and connectivity (https://sci-hub.se/10.1371/journal.pone.0112691)

# Studies on IQ/G-factor & Creativity/Divergent thinking
- Working memory is (almost) perfectly predicted by g https://www.sciencedirect.com/science/article/abs/pii/S0160289604000030
- the incorporated [meaning those working on businesses requiring high cognitive performance] - as teenagers - typically scored higher on learning aptitude tests, had greater self-esteem, and engaged in more disruptive, illicit activities. https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.697.3183&rep=rep1&type=pdf 
- People who score high on intelligence tests are also more likely to be libertarians. That's a minority viewpoint even among the intelligentsia and not what we would expect if IQ correlated with conformity. http://econfaculty.gmu.edu/bcaplan/pdfs/intelligencethinklike.pdf
- intelligence is positively associated with people’s propensity to take risk in a sample of 11,000 twins. This was true of risk seeking behavior in general as well as risk seeking behavior specifically with reference to finances. https://sci-hub.se/10.1007/s11166-017-9261-3
- IQ does correlate with RQ significantly (~0.7!) - people really just like to bash IQ? https://sci-hub.se/https://doi.org/10.1016/j.intell.2017.01.001
- g factor explained 36.889% of the relationship between analogical, analytical and creative thinking https://sci-hub.se/https://doi.org/10.1016/j.tsc.2014.03.006
- intelligence and creativity may be based on two different mental operations, although intelligence tests and creativity tests have significant correlations. the association between intelligence tests and creativity tests may be made because it is possible to use crystallized knowledge as a resource for the mental operation of creativity. https://sci-hub.se/https://doi.org/10.1002/j.2162-6057.2010.tb01329.x
- Multiple regression analyses showed that openness and intelligence, but not schizotypy, predicted reliable observer ratings of verbal and drawing creativity. Thus, the ‘madness-creativity’ link seems mediated by the personality trait of openness https://sci-hub.se/https://doi.org/10.1016/j.schres.2007.02.007
-  a decrease of DT/IQ correlations (a) with increasing age and (b) among grammar school vs. secondary school girls https://sci-hub.se/https://doi.org/10.1080/00220973.1978.11011647
- link between IQ test scores and creative achievement: r = .167 [but DT tests better at .216]  https://sci-hub.se/https://doi.org/10.1002/j.2162-6057.2008.tb01290.x
- [Added 2020-10-26] When investigating a liberal criterion of ideational originality (i.e., two original ideas), a threshold was detected at around 100 IQ points. In contrast, a threshold of 120 IQ points emerged when the criterion was more demanding (i.e., many original ideas). Moreover, an IQ of around 85 IQ points was found to form the threshold for a purely quantitative measure of creative potential (i.e., ideational fluency). [...] no threshold was found for creative achievement, i.e. creative achievement benefits from higher intelligence even at fairly high levels of intellectual ability. https://www.sciencedirect.com/science/article/pii/S016028961300024X