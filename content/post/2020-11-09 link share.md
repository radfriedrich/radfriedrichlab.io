+++
title = "2020-11-09 link share"
date = 2020-11-09
description = "best of my recent online reads"
tags = ["goodreads"]
+++

# Last week's top 5:

* **[The Narrowing Circle](https://www.gwern.net/The-Narrowing-Circle)**
> The “expanding circle” historical thesis ignores all instances in which modern ethics narrowed the set of beings to be morally regarded, often backing its exclusion by asserting their non-existence, and thus assumes its conclusion: where the circle is expanded, it’s highlighted as moral ‘progress’, and where it is narrowed, what is outside is simply defined away. When one compares modern with ancient society, the religious differences are striking: almost every single supernatural entity (place, personage, or force) has been excluded from the circle of moral concern, where they used to be huge parts of the circle and one could almost say the entire circle. Further examples include estates, houses, fetuses, prisoners, and graves.

* **[AGI safety from first principles](https://www.alignmentforum.org/s/mzgtmmTKKn5MuCzFJ)**
> In this report I have attempted to put together the most compelling case for why the development of AGI might pose an existential threat. It stems from my dissatisfaction with existing arguments about the potential risks from AGI. Early work tends to be less relevant in the context of modern machine learning; more recent work is scattered and brief. I originally intended to just summarise other people's arguments, but as this report has grown, it's become more representative of my own views and less representative of anyone else's. So while it covers the standard ideas, I also think that it provides a new perspective on how to think about AGI - one which doesn't take any previous claims for granted, but attempts to work them out from first principles.

* **[Evolved Altruism, Strong Reciprocity, and Perception of Risk](https://sci-hub.se/10.1196/annals.1399.012)**
> Humans have a long history of coping with particular recurring risks. We expect natural selection to have resulted in specific physiological and psychological adaptations that respond well to these risks. Why, then, does it seem so difficult to communicate risk? We suggest that the human mind has been structured by natural selection to use a mental calculus for reckoning uncertainty and making decisions in the face of risk that can be substantially different from probability theory, propositional calculus (logic), or economic rationality (utility maximization). We argue that this is because of the unique armamentarium of strategies humans have evolved to cope with the risks faced during our long history living as hunter–gatherers. In particular, we believe the risk of social contract violation (not contributing a fair share to cooperative endeavors) was an important selective factor because reciprocity, reciprocal altruism, and cooperation are primary adaptations to the most important risks our ancestors faced.

* **[Sustaining cooperation in laboratory public goods experiments: a selective survey of the literature](https://link.springer.com/article/10.1007/s10683-010-9257-1)**
> Many participants in laboratory public goods experiments are “conditional cooperators” whose contributions to the public good are positively correlated with their beliefs about the average group contribution. Conditional cooperators are often able to sustain high contributions to the public good through costly monetary punishment of free-riders but also by other mechanisms such as expressions of disapproval, advice giving and assortative matching.

* **[AI Governance: Opportunity and Theory of Impact](https://forum.effectivealtruism.org/posts/42reWndoTEhFqu6T8/ai-governance-opportunity-and-theory-of-impact)**
> I believe advances in AI are likely to be among the most impactful global developments in the coming decades, and that AI governance will become among the most important global issue areas. AI governance is a new field and is relatively neglected. I’ll explain here how I think about this as a cause area and my perspective on how best to pursue positive impact in this space. The value of investing in this field can be appreciated whether one is primarily concerned with contemporary policy challenges or long-term risks and opportunities (“longtermism”); this piece is primarily aimed at a longtermist perspective. Differing from some other longtermist work on AI, I emphasize the importance of also preparing for more conventional scenarios of AI development.

# and boni:

* **[Is China the Governance of the Future?](https://quillette.com/2020/10/10/is-china-the-governance-of-the-future/)**
> Have the West’s electorates become so disappointed and distrustful of [democratic] values and practices — so spoiled, so hooked on pleasure and entertainment—that they are a pushover for the disciplined cadres and cowed populations of the authoritarian states? Is an authoritarian state, a Leviathan capable of suppression of conflicts, the only recourse we will soon have? It is not a silly question: But even now, it is hard to believe that the answer to these questions is a dispirited “yes.”

* **[Twitter thread from Aella:](https://twitter.com/Aella_Girl/status/1323404266426359809)**
> I'm going through my newest data set (~3300 answers) and it's absolutely fascinating. I asked about 'who do you know', with stronger weights for knowing someone better. Some finds so far:

* **[The case for taking AI seriously as a threat to humanity](https://www.vox.com/future-perfect/2018/12/21/18126576/ai-artificial-intelligence-machine-learning-safety-alignment)**
> The conversation about AI is full of confusion, misinformation, and people talking past each other — in large part because we use the word “AI” to refer to so many things. So here’s the big picture on how artificial intelligence might pose a catastrophic danger, in nine questions:

* **[Hedgemony - A Game of Strategic Choices](https://www.rand.org/pubs/tools/TL301.html)**
> RAND researchers developed Hedgemony, a wargame designed to teach U.S. defense professionals how different strategies could affect key planning factors in the trade space at the intersection of force development, force management, force posture, and force employment. The game presents players, representing the United States and its key strategic partners and competitors, with a global situation, competing national incentives, constraints, and objectives; a set of military forces with defined capacities and capabilities; and a pool of periodically renewable resources. The players are asked to outline their strategies and are then challenged to make difficult choices by managing the allocation of resources and forces in alignment with their strategies to accomplish their objectives within resource and time constraints.

* **[What the Hell is Going On - Summary](https://www.will-mannon.com/what-the-hell-is-going-on-summary)**
> David Perell’s essay What the Hell is Going On  outlines how the shift from information scarcity to information abundance has upended commerce, education, and politics. It’s also over 13,000 words long. I’ve distilled the core arguments below: 

* **[Differences in the Intensity of Valenced Experience across Species](https://forum.effectivealtruism.org/posts/H7KMqMtqNifGYMDft/differences-in-the-intensity-of-valenced-experience-across)**
> Humans and nonhuman mammals possess neurologically and behaviorally similar affective systems, suggesting that most mammals are capable of experiencing roughly the same base set of emotions. It’s unclear how stark the differences in cognitive sophistication are across mammalian species and how these differences might affect the intensity of valenced experience. Given these facts, it would appear that we are more justified in thinking that humans possess an intensity range roughly similar to other mammals than we are in thinking that humans possess a much wider range than other mammals. It is unclear how mammals compare to other groups of animals, in part because there is a much sparser scientific literature on the capabilities of non-mammals and in part because it is unclear how increasing phylogenetic distance ought to influence reasoning-by-analogy about subjective experience.

* **[I am pretty positive this is a joke, but it's an elaborate one](https://www.appliedeschatology.com/)**
> The Centre for Applied Eschatology is a transdisciplinary research center dedicated to ending the world. We connect professionals from the public sector, private industry, and academia to develop new knowledge and apply existing research to curtail the world’s long-term future. 

* **[Rob Wiblin on facebook:](https://www.facebook.com/story.php?story_fbid=911717570565&id=204401235)**
> The Trump campaign called a press conference to complain about election fraud but they accidentally booked it for Four Seasons Total Landscaping rather than Four Seasons Hotel. They just went ahead so all the press were crowded in the parking lot of the landscaping company next to an adult book store. These folks are running the US coronavirus response

* **[Are We Living At The Hinge Of History?](https://static1.squarespace.com/static/5506078de4b02d88372eee4e/t/5f36b015d9a3691ba8e1096b/1597419543571/Are+we+living+at+the+hinge+of+history.pdf)**
> In this article I try to make the hinge of history claim more precise, give arguments in favour and against, and assess whether it is true. Ultimately, I argue that the claim (as I construe it, which might be quite far from any claim Parfit would endorse) is quite unlikely to be true, and that this fact can serve as part of an argument for the conclusion that impartial altruists should generally be investing their resources, rather than trying to do good immediately.

* **[Thoughts on whether we're living at the most influential time in history](https://forum.effectivealtruism.org/posts/j8afBEAa7Xb2R9AZN/thoughts-on-whether-we-re-living-at-the-most-influential)**
> I think Will’s arguments mostly lead to believing that you aren’t an “early human” (a human who lives on Earth before humanity colonizes the universe and flourishes for a long time) rather than believing that early humans aren’t hugely influential, so you conclude that either humanity doesn’t have a long future or that you probably live in a simulation.