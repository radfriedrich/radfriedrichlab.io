+++
title = "2020-11-02 link share"
date = 2020-11-02
description = "best of my recent online reads"
tags = ["goodreads"]
+++

# Last week's top 5:

* **[Are we experiencing unprecedented levels of institutional failure or unprecedented levels of transparency into prevailing competence levels?](https://mobile.twitter.com/patio11/status/1318865677264904195)**
> I think I believe ["less competent than in the past"] about institutions I am an outsider about but I do not believe it about institutions I was an insider of, and this has me in a state of heavy epistemic uncertainty on the matter.

* **[Gossip at scale](https://www.arnoldkling.com/blog/gossip-at-scale/)**
> Skills that gave one status in Analog City, including the use of reason and the scientific method to sift through evidence, are less effective in achieving status in the gossip-laden environment of Digital City. Meanwhile, skill at dressing up rumors and creating what Jason Riley termed “poetic truth” (i.e., a falsehood that is appealing to believe) has increased in effectiveness.

* **[The epistemic uncertainty of COVID-19: failures and successes of heuristics in clinical decision-making](https://link.springer.com/article/10.1007/s11299-020-00262-0)**
> The brief article deals with the following questions: Was the adaptive toolbox of heuristics ecologically rational and specifically accurate in the initial stages of COVID-19, which was characterized by epistemic uncertainty? In other words, in dealing with COVID-19 did the environmental structural variables allow the success of a given heuristic strategy?

* **[From Existential Angst To Existential Hope](https://www.existentialhope.com/angst)**
> Teetering here on the fulcrum of destiny stands our own bemused species. The future of the universe hinges on what we do next. If we take up the sacred fire, and stride forth into space as the torchbearers of Life, this universe will be aborning. Because of us, the barren dust dusts of a million billion worlds will coil up into the pulsing magic forms of animate matter. Slag will become soil, grass will sprout, and forests will spring up in once sterile places — a whole frozen universe will thaw and transmogrify, from howling desolation into a blossoming paradise. Dust into Life; the very alchemy of God.

* **[GPT-3: Its Nature, Scope, Limits, and Consequences](https://link.springer.com/article/10.1007/s11023-020-09548-1)**
> In this commentary, we discuss the nature of reversible and irreversible questions, that is, questions that may enable one to identify the nature of the source of their answers. We then introduce GPT-3, a third-generation, autoregressive language model that uses deep learning to produce human-like texts, and use the previous distinction to analyse it. We expand the analysis to present three tests based on mathematical, semantic (that is, the Turing Test), and ethical questions and show that GPT-3 is not designed to pass any of them. This is a reminder that GPT-3 does not do what it is not supposed to do, and that any interpretation of GPT-3 as the beginning of the emergence of a general form of artificial intelligence is merely uninformed science fiction. We conclude by outlining some of the significant consequences of the industrialisation of automatic and cheap production of good, semantic artefacts.

# and boni:

* **[The end of the Bronze Age as an example of a sudden collapse of civilization](https://forum.effectivealtruism.org/posts/Zbvsfe9dDPKoy52QA/the-end-of-the-bronze-age-as-an-example-of-a-sudden-collapse)**
> Imagine a person that was born 1230 BC in Ugarit, a prosperous city of the vast Hittite Empire. Everything seems to be going well. The cisterns and granaries are full, goods and people from all over the known world regularly arrive at the city and a surprisingly large portion of the population is able to write. Now fast forward 80 years to 1150 BC. Ugarit is a blackened ruin, the Hittite Empire simply does not exist anymore, everyone she knew is dead from disease, war and famine and the sun has not shown through the grey clouds for years. 

* **[Intro to Effective Altruism Syllabus by EA Brown](https://docs.google.com/document/d/1_ZufqzAZjyIlOxQPvP55fiSEqwHgGPwgM5bFBNXRloI/edit)**

* **[In-depth fellowship program of Effective Altruism Brown](https://brownea.org/In-Depth-Fellowship)** - great overview of some of the most up-to-date thinking in the EA community.

* **[The Bisexual Woman’s Guide to Dating Women](https://putanumonit.com/2020/01/20/bisexual-womans-guide/)**
> Just for reference, this is how most straight men feel when online dating, so expect to encounter the same. Tinder online experiments and academic studies show that similarly attractive men compared to women get far fewer messages and matches. So in order for them to find someone they HAVE to message first. Additionally, those who message (or approach) you first, often think you are more attractive than themselves. As such it also makes strategic sense to move first.

* **[SubOnlyStackFans](https://putanumonit.com/2020/10/25/subonlystackfans/)**
> I think it’s better for talented people to pick the platform that best suits their talents and let it be their job resume, dating profile, and friend-attractor, rather than squeezing themselves into a Tinder or LinkedIn box. For some it will be a blog of personal website, for some Twitter or Instagram.

* **[HN comments: Study helps explain why motivation to learn declines with age](https://news.ycombinator.com/item?id=24925135)** - just nice stories of "old" people continuing to grow.
