+++
title = "Do we need a special forces unit for public policy?"
date = 2020-05-27
description = "first draft of one set of thoughts on governance of catastrophic risks"
tags = ["policymaking", "longtermgov"]
+++

[Epistemic status: musings without second thought triggered by COVID governance]

In many high-stakes risk contexts, humans have managed to develop surge capacity to cope with (the threat of) extreme shocks. Whether it's police, military or the private sector. Any reasonable institution has reserves and almost any institution has people specifically trained to manage the reserves once they need to be deployed. Usually, the reserves themselves need to be maintained, and even that redundancy seems to be affordable for many.

There is no reason not to have a special force unit for policy-making - experts trained to reason in contexts of extreme risks. Thinking about scenarios where normal distributions fail to represent reality is substantially different from reasoning about scenarios where efficiency is king because downside risks are low. Human minds suck at shifting gears - and it might not even be effective to expect people to shift. It might just be much more efficient to have different people work in different contexts.

Governments do not seem to differentiate along the most useful contextual lines to encourage specialization in the most relevant ways. We are dividing expertise by industrial sectors - a good first step. The next best step to take, however, is the division of issues within each sector into
(i) everyday governance of business-as-usual sector activities (just the usual good governance stuff); and
(ii) extreme outlier management.

This is in part happening with the rise of foresight units but the bureaucrats having to deal with the intelligence gained from foresight activities have not changed. Sure, trends have to be accounted for in everyday-policy-making. But trend-lines also demonstrate their inherent uncertainty that is mostly not being dealt with. When we think something is likely to happen, that might just mean that in more than 50% of all possible worlds, it will.

It's good for many sectors to just take the bet on the most likely outcome to happen, keep calm and carry on. But what if that's not what's going to happen? Imagine there's a 49% chance of something going horribly wrong. We should be prepared to shift gears quickly. Sometimes, even the most probable option might only have 20% likelihood and the rest of the probability mass is spread across various options marginally below 20%.

While most of the world continues business as usual, we need powerful units to prepare shifts of most of that activity in case of substantial threats to it. Ruthlessly applying the 80/20 principle to governance, this might mean that 80% of our government should be acting as if no major uncertainties were ahead. 20% of our government capacity would then have to prepare for worst case threats and less existential but unexpected shifts.

A key here is for the business-as-usual governors to be aware of the value of the worst-case governors. As soon as things shift faster than anticipated, special forces have to complement governance with a whole different mindset: the triage and safety mindset. 

(usually, policy makers can be more risk-taking and simply work with what's within the overton window)