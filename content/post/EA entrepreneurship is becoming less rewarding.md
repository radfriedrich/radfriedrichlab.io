+++
title = "EA-entrepreneurship is becoming less rewarding"
date = 2020-07-09
description = "a reaction to something"
tags = ["EA"]
+++

_[Epistemic status: patchwork of unsystematically tracked personal impressions since first encountering EA in 2014 put together over the course of a day]_

# Context
This post grew out of a knee-jerk reaction I had to Rob Wiblin's "[Consider a wider range of jobs, paths and problems if you want to improve the long-term future](https://forum.effectivealtruism.org/posts/vHPR95Gnsa3Gkgjof/consider-a-wider-range-of-jobs-paths-and-problems-if-you)". The post is mostly about considering employment opportunities beyond those hyped by the EA community.^[A point I fully agree with. There are many communities on this globe genuinely trying to figure things out. Even though the EA community stands out in terms of epistemic norms, EAs can and will have to have disproportionate impact outside of EA organizations.]

While reading, though, one thing struck me as implicitly propagating a dangerous half-truth that merits some more attention: _EAs should be less risk-averse and start more independent projects._ The sentence that made me think it's worth writing up my reaction was:

> I suggest paying a bit more respect to the courage or initiative shown by those who choose to figure out their own unique path or otherwise do something different than those around them.

Given the entrepreneurial slant of EA culture, I think it's reasonable to suppose that many people will end up reading "we should celebrate risk-taking even more than we already do".^[I realize that it was probably meant as "we should pay more respect to aspiring EAs who are pursuing promising career paths that aren't explicitly endorsed by the community".] In that case, I think we're likely wasting talent by slowing down and upsetting people who don't naturally think through social dynamics as much as some of us may.

So here's an attempt at explaining why it's getting more difficult to fulfil the necessary conditions to start independent EA projects _without burning "EA" career capital_ (and why that might have been different in the early days).

# tl;dr

With the increasing establishment of any field, the likelihood of success of new projects decreases while the gravitas of failure for first-time founders increases. If you aren't yet part of one of the core groups that allocate the necessary resources for low-risk experimentation, I would build career capital via safer paths first.

# Trust-based networks scale badly

Networks provide access to their resources in a mostly trust-based manner. ^[Resources are often allocated in more formalised ways, but to be considered for allocation usually seems to be a trust-heavy process.]

## Verification of alignment is always costly

The ways in which a trust-based network can grow are limited if it is built around values: either through explicit entry criteria or through recommender-systems. Designing explicit entry criteria that work is _hard,_ so our civilization mostly relies on opaque arbitration systems running on human brains.

EA organizations and thought leaders do an extraordinary job at documenting their thinking. But no matter how well you have studied it, verification of your feat remains resource-intense for the network.

## Core groups are increasingly hard to access

As an EA project founder, you have to make verification of your alignment as cheap as possible. That's costly for a resource-strapped project in its starting phase. Thus, you want to get maximally relevant feedback - ideally from those who can verify you.

But if you have a lot of "EA capital" and get asked for feedback from someone you don't know, you are unlikely to get involved because you don't have enough time to properly vet them and their project. Being even vaguely associated with it through mere engagement could suggest you endorse it - and you have better things to do than to create a nuanced write-up explaining your engagement and lessons learned from it.

As a result, the important EA people will not engage with _cool new independent project_ unless the founder has sufficient "EA capital" prior to going "off track".

## Losses look worse in relation to safe bets

Even with relevant feedback it is difficult to build up a successful project. And without the trust of the core group, no one will recognize your attempt in case of failure.

Unless you are already highly skilled or have a lot of support, you're likely to fail at a stage which doesn't provide much information on your skills. In that case, any failure provides some information regarding your skill level.

As there are more and more smart youngsters, it gets more and more difficult not to be burnt by failure simply because failure looks worse _relative to everyone else_ playing it safe or already having succeeded.

## EA used to be different because it was young

In the early days of EA, a handful of smart youngsters could start a bunch of projects because everyone knew everyone else. There was a lot of free space and excitement about people exploring it. Since then, the network has grown, the approach become prudent and the space carved out.

While the culture of openness has scaled well, I worry we might not acknowledge clearly enough the fact that trust-based networks don't.

There still seems to be a myth around EA entrepreneurship - even though simply starting an EA project is getting you less and less "EA capital" and structures become more and more institutionalized. This is not a bad thing; it's a sign of maturity. I just worry that it hasn't been made explicit often enough:

You're more likely to fail starting a new EA project and also more likely not to get credit for trying nowadays because it's harder to interact with the key nodes of the network, as they now have to protect themselves more vigilantly from incurring significant opportunity cost.

# Bottom line

It's not worth going off the beaten track for most people.^[Even more so if being part of EA is already considered weird in your family/social circles. Not having a "proper" job might cost you too many weirdness points to have a larger impact later on in life (when you'd likely have most of your impact).]

Given EA demographics, there only are few highly skilled or wealthy people who can afford to resist these incentive structures. Most are better off continuing on the beaten paths until they have accumulated enough capital to not lose status when losing lopsided bets - no matter how well calculated.

I would be interested in hearing ideas on scaling more smoothly.