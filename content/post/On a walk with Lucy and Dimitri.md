+++
title = "On a walk with Lucy and Dimitri"
date = 2020-05-27
description = "somebody told me this"
tags = ["psychedelics"]
+++

[Epistemic status: just "no". somebody's notes while on psychedelic substances, found them entertaining]

Wir haben den Rundweg erfunden um vorzumachen, wir würden irgendwo hin gehen, selbst wenn wir eigentlich *nur gehen* wollen. Oder wir geben vor auf dem Weg zu sein obwohl wir eigentlich nur sein wollen.
---

Can you tell whether people are walking their dogs because they have to, or because they want to?

---
I feel more comfortable turning around behind someone than in front of someone to not appear too strange.
---

We keep walking further even though we've arrived. Just because we don't want other people to watch where we've arrived? Because we arrive only where others aren't?

When do our closest relatives become like dogs?

The question is when do we let them become dogs? When do they let us become dogs? How can we help each other not to become dogs in the unwanted burden sense?

We only go off track when we can't cater to some of our most basic, functional needs. What if we didn't have them?

---
Why do I not go where I want to when I think others are watching? Do I not want them to judge? Not go where I go? Not know where I go? Am I even sure I know where I'm going? That I want to go where I think I want to? Maybe I don't go there because I know going there immediately would make it undesirable? Or because the detour might be worth it?

Why do I feel like the attention is on me, even though everyone else is just trying to be, just like me? How many people are really just trying to be though?

When I don't think I have the status to clearly assess what's going on, I shy away from the assessment and let external forces rule me.
---

Remember the beautiful patchy hedge.

---
What if we just constantly patch a buggy reality? What if that's perfectly fine because the necessary resources are abundant? What if that's the only way? What if that's life? What if that's consciousness? What if that's all that matters - beautifully patching bugs and copy-pasting code until it works?
---

Why do we look away when walking by even though we just want to understand what's going on or acknowledge each others existence? Why is it so hard to strike the right balance between intruding and acknowledging? Where acknowledgment empowers, intruding hinders.

---
There's not enough room for all of us. We cannot be all we are at any single point. But why? What is lost when we're all, all all?
